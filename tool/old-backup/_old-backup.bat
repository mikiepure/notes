@ECHO OFF

REM # -------------------

REM # Set directory name of backup
SET DIR_NAME_BACKUP=_old

REM # -------------------

ECHO "-- Start Batch File [%~nx0] --"

REM # Set directory path of backup
SET DIR_PATH_BACKUP=%~dp0%DIR_NAME_BACKUP%\

REM # Create backup directory
IF NOT EXIST "%DIR_PATH_BACKUP%" (
  REM # Create directory (if directory does not exist)
  ECHO "[I] Create directory ..."
  ECHO "   > Target: %DIR_PATH_BACKUP%"
  MKDIR "%DIR_PATH_BACKUP%"
  
  REM # Check creating directory is succeeded
  IF {%ERRORLEVEL%} == {0} (
    ECHO "[I] Create directory is succeeded."
  ) ELSE (
    ECHO "[E] Create directory is failed."
    ECHO "[E] Abort batch file."
    PAUSE
    EXIT
  )
)

:START

REM # if arguments is blank, exit batch file
IF "%1" == "" (
  GOTO END
)

REM # Set variables of file information
SET FILE_PATH=%~f1
SET FILE_NAME=%~n1
SET FILE_EXT=%~x1
SET FILE_DT=%~t1

REM # Set variables of date time
SET DATE_Y=%FILE_DT:~0,4%
SET DATE_M=%FILE_DT:~5,2%
SET DATE_D=%FILE_DT:~8,2%
SET TIME_H=%FILE_DT:~11,2%
SET TIME_M=%FILE_DT:~14,2%

REM # Create filename added datetime (ex: FILENAME_yyyymmdd-HHMM.doc)
SET FILE_NAME_NEW=%FILE_NAME%_%DATE_Y%%DATE_M%%DATE_D%-%TIME_H%%TIME_M%%FILE_EXT%

REM # Copy file to backup
ECHO "[I] Copy file ..."
ECHO "   > Src: %FILE_PATH%"
ECHO "   > Dst: %DIR_PATH_BACKUP%%FILE_NAME_NEW%"
COPY "%FILE_PATH%" "%DIR_PATH_BACKUP%%FILE_NAME_NEW%"

REM # Check copying file is succeeded
IF {%ERRORLEVEL%} == {0} (
  ECHO "[I] Copy file is succeeded."
) ELSE (
  ECHO "[E] Copy file is failed."
  ECHO "[E] Abort batch file."
  PAUSE
  EXIT
)

REM # Shift batch file parameter (for next arguments)
SHIFT
GOTO START

:END
ECHO "-- Exit Batch File --"
REM # PAUSE
